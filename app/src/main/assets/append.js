function setCreds(cpf, passwd) {
  let cpfInput = document.getElementsByTagName("input")[0];
  let passwdInput = document.getElementsByTagName("input")[1];
  cpfInput.value = cpf;
  passwdInput.value = passwd;
  cpfInput.dispatchEvent(new Event("input", { bubbles: true, cancelable: true }));
  passwdInput.dispatchEvent(new Event("input", { bubbles: true, cancelable: true }));
  cpfInput.parentElement.parentElement.parentElement.style.display = "none";
  passwdInput.parentElement.parentElement.parentElement.style.display = "none";
}

function setPageLoginWithLike() {
  let identifyText = document.querySelector(".col-md-12.e-card-header-title h3");
  identifyText.textContent = "Carteirinha Virtual";
  let descriptionText = document.querySelector(".col-md-12.e-card-sub-title");
  descriptionText.innerHTML = "Acesso requer captcha.<br>Complete e selecione \"Entrar\" para prosseguir";
  let header = document.getElementById("header");
  header.remove();
}

function loginProcedure(cpf, passwd) {
   setPageLoginWithLike();
   setCreds(cpf, passwd);
}

function fetchUserDetails() {
    return document.querySelector("h4.text-center").innerText;
}