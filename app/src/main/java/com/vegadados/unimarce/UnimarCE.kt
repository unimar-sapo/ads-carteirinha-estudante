package com.vegadados.unimarce

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class UnimarCE : Application()