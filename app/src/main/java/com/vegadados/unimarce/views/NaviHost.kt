package com.vegadados.unimarce.views

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.vegadados.unimarce.views.camera.CameraScreen
import com.vegadados.unimarce.views.carteirinha.CarteirinhaScreen
import com.vegadados.unimarce.views.home.HomeScreen
import com.vegadados.unimarce.views.login.LoginScreen

object Destinations {
    const val Login = "login"
    const val Home = "home"
    const val Camera = "camera"
    const val Carteirinha = "carteirinha"
    const val Scanner = "scanner"
}

@Composable
fun NaviHost(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    startDestination: String = Destinations.Login
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier
    ) {
        composable(Destinations.Login) {
            LoginScreen(
                navigateHomeScreen = { navController.navigate(Destinations.Home) },
                navCameraScreen = { navController.navigate(Destinations.Camera) },
                navCarteirinhaScreen = { navController.navigate(Destinations.Carteirinha) },
                navScanQr = { navController.navigate(Destinations.Scanner) }
            )
        }

        composable(Destinations.Home) {
            HomeScreen(
                navigateLogin = { navController.navigate(Destinations.Login) },
                navigateTakePic = { navController.navigate(Destinations.Camera) },
                navigateCarteirinha = { navController.navigate(Destinations.Carteirinha) }
            )
        }

        composable(Destinations.Camera) {
            CameraScreen(
                navigateUp = { navController.navigateUp() }
            )
        }

        composable(Destinations.Carteirinha) {
            CarteirinhaScreen()
        }

        composable(Destinations.Scanner) {
            CameraScreen(
                isScanQr = true,
                navigateUp = { navController.navigateUp() }
            )
        }
    }
}