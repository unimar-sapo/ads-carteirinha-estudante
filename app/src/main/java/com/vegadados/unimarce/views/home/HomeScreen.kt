@file:OptIn(ExperimentalMaterial3Api::class)

package com.vegadados.unimarce.views.home

import android.app.Activity
import android.content.pm.ActivityInfo
import android.net.Uri
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Badge
import androidx.compose.material.icons.outlined.Camera
import androidx.compose.material.icons.outlined.Description
import androidx.compose.material.icons.outlined.ExitToApp
import androidx.compose.material.icons.outlined.HistoryEdu
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Person
import androidx.compose.material.icons.outlined.School
import androidx.compose.material.icons.outlined.Wallet
import androidx.compose.material.icons.outlined.WarningAmber
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.vegadados.unimarce.R
import com.vegadados.unimarce.components.LoadingOverlay
import kotlin.system.exitProcess

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    homeVm: HomeScreenViewModel = hiltViewModel(),
    navigateLogin: () -> Unit,
    navigateTakePic: () -> Unit,
    navigateCarteirinha: () -> Unit,
) {
    val uiState by homeVm.uiState.collectAsState()
    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()

    val ctx = LocalContext.current
    val activity = ctx as Activity

    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR

    LaunchedEffect(Unit) {
        if (homeVm.userSession.cachedPicture != Uri.EMPTY)
            homeVm.setupFirstAccess()
    }

    BaseScreen(
        scrollBehavior = scrollBehavior,
        title = "Carteirinha"
    ) {
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            Spacer(modifier = Modifier.padding(4.dp))
            Box(modifier = Modifier.padding(start = 24.dp, end = 24.dp)) {
                Image(
                    modifier = Modifier
                        .clickable {
                            if (!uiState.firstAccess) homeVm.onRequestVisualizarCarteirinha(
                                onFinishRequest = { navigateCarteirinha() })
                        }
                        .fillMaxWidth(),
                    painter = if (uiState.firstAccess) painterResource(R.drawable.card_min_un) else painterResource(
                        R.drawable.card_min
                    ),
                    contentDescription = ""
                )
            }
            Spacer(modifier = Modifier.padding(4.dp))
            Text(
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                text = if (uiState.firstAccess) "Carteirinha indisponível" else "Toque acima para acessar a carteirinha virtual",
                style = MaterialTheme.typography.bodySmall
            )
            Spacer(modifier = Modifier.padding(4.dp))
            Column(modifier = Modifier.padding(start = 24.dp, end = 24.dp)) {
                if (uiState.firstAccess)
                    CustomCard(title = "Atenção", imageVector = Icons.Outlined.WarningAmber) {
                        Text(
                            fontSize = 16.sp,
                            text = "Você precisa definir uma foto de perfil para continuar com o uso da carteirinha virtual, toque no botão a baixo para definir uma foto de perfil e ter acesso a carteirinha do estudante."
                        )
                        Row {
                            Spacer(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .weight(1F)
                            )
                            Button(
                                onClick = { navigateTakePic() }) {
                                Text(text = "Tirar foto agora")
                            }
                        }
                    }
                Spacer(modifier = Modifier.padding(4.dp))
                CustomCard(title = "Dados", imageVector = Icons.Outlined.Info) {
                    if (uiState.pictureUrl.isNotEmpty()) {
                        Box(
                            Modifier
                                .fillMaxWidth()
                                .padding(12.dp)
                                .align(Alignment.CenterHorizontally)
                        ) {
                            Box(Modifier.align(Alignment.Center)) {
                                AsyncImage(
                                    modifier = Modifier
                                        .size(128.dp)
                                        .clip(CircleShape)
                                        .border(
                                            2.dp,
                                            MaterialTheme.colorScheme.primary,
                                            CircleShape
                                        ),
                                    model = uiState.pictureUrl,
                                    contentScale = ContentScale.Crop,
                                    contentDescription = null,
                                )
                            }
                            Spacer(modifier = Modifier.padding(4.dp))
                        }
                    }
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Spacer(modifier = Modifier.padding(4.dp))
                        Column {
                            CardItem(Icons.Outlined.Person, "Nome:", uiState.name)
                            CardItem(Icons.Outlined.Badge, "RA:", uiState.ra)
                            CardItem(Icons.Outlined.School, "Curso:", uiState.course)
                            CardItem(
                                Icons.Outlined.HistoryEdu,
                                "ID do curso:",
                                "" + uiState.courseId
                            )
                            CardItem(Icons.Outlined.Description, "Ano:", uiState.classYear)
                        }
                    }
                }
                Spacer(modifier = Modifier.padding(4.dp))
            }
        }
    }

    if (uiState.isLoadingOverlayVisible)
        LoadingOverlay()

    if (uiState.hasPressedBack)
        AlertDialog(onDismissRequest = { homeVm.onToggleBack() }) {
            AlertDialog(
                icon = {
                    Icon(Icons.Outlined.ExitToApp, contentDescription = "Exit")
                },
                title = {
                    Text(text = "Fechar")
                },
                text = {
                    Text(text = "Você realmente quer fechar o app?")
                },
                onDismissRequest = {
                    homeVm.onToggleBack()
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            exitProcess(0)
                        }
                    ) {
                        Text("Finalizar app")
                    }
                }
            )

        }

    BackHandler {
        homeVm.onToggleBack()
    }
}

@Composable
fun CustomCard(
    title: String,
    imageVector: ImageVector,
    content: @Composable () -> Unit
) {
    Card {
        Column(modifier = Modifier.padding(12.dp)) {
            Spacer(modifier = Modifier.padding(2.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    imageVector = imageVector,
                    contentDescription = null
                )
                Spacer(modifier = Modifier.padding(4.dp))
                Text(
                    text = title,
                    fontSize = 20.sp,
                    color = MaterialTheme.colorScheme.onSurface
                )
            }
            Spacer(modifier = Modifier.padding(4.dp))
            Divider(
                color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.1f),
                thickness = 1.dp
            )
            Spacer(modifier = Modifier.padding(4.dp))
            content()
        }
    }
}

@Composable
fun CardItem(
    icon: ImageVector? = null,
    title: String,
    text: String
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        if (icon != null)
            Icon(imageVector = icon, contentDescription = null)
        Spacer(modifier = Modifier.padding(4.dp))
        Column {
            Text(text = title, fontSize = 12.sp, maxLines = 1)
            Text(text = text, fontSize = 16.sp, maxLines = 1)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BaseScreen(
    modifier: Modifier = Modifier,
    scrollBehavior: TopAppBarScrollBehavior,
    title: String = "",
    content: @Composable () -> Unit,
) {
    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            TopAppBar(
                scrollBehavior = scrollBehavior,
                title = {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Icon(imageVector = Icons.Outlined.Wallet, contentDescription = "")
                        Spacer(modifier = Modifier.padding(4.dp))
                        Text(title)
                    }
                }
            )
        },
    ) { innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)) {
            content()
        }
    }
}
