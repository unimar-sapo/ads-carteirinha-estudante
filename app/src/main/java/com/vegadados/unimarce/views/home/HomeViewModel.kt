package com.vegadados.unimarce.views.home

import android.app.Application
import android.net.Uri
import androidx.core.net.toFile
import androidx.core.text.htmlEncode
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vegadados.unimarce.BaseWebView
import com.vegadados.unimarce.Constants
import com.vegadados.unimarce.Constants.ENDPOINT_CERTIFICATE
import com.vegadados.unimarce.Constants.ENDPOINT_NEW_USER
import com.vegadados.unimarce.Constants.ENDPOINT_USER_DETAILS
import com.vegadados.unimarce.UserSession
import com.vegadados.unimarce.log
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.asRequestBody
import javax.inject.Inject


@HiltViewModel
class HomeScreenViewModel
@Inject constructor(
    val app: Application,
    val webView: BaseWebView,
    val userSession: UserSession,
    val okHttpClient: OkHttpClient
) : ViewModel() {

    private val _uiState = MutableStateFlow(
        HomeScreenState(
            ra = userSession.ra,
            name = userSession.name,
            course = userSession.course,
            classYear = userSession.classYear,
            courseId = userSession.courseId
        )
    )
    val uiState: StateFlow<HomeScreenState> = _uiState.asStateFlow()

    init {
        log("loadedSession: $userSession")
        remoteCheckUserDetails()
    }

    fun remoteCheckUserDetails() {
        val ra = userSession.ra
        val url = ENDPOINT_USER_DETAILS.format(ra)
        val request = Request.Builder().url(url).build()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = okHttpClient.newCall(request).execute()
                log("$url -> " + response.code)
                when (response.code) {
                    200 -> {
                        loadPfp()
                    }

                    500 -> {
                        _uiState.update { it.copy(firstAccess = true) }
                    }

                    else -> {} // likely error
                }
                _uiState.update { it.copy(isLoadingOverlayVisible = false) }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    var firstAccessLock = false

    fun setupFirstAccess() {
        _uiState.update { it.copy(isLoadingOverlayVisible = true) }
        if (firstAccessLock) return
        firstAccessLock = true

        val ra = userSession.ra
        val name = userSession.name
        val course = userSession.course
        val courseYear = userSession.classYear
        val courseId = userSession.courseId

        val newUserEndpoint = ENDPOINT_NEW_USER.format(
            ra.htmlEncode(),
            name.htmlEncode(),
            course.htmlEncode(),
            courseYear.htmlEncode(),
            courseId
        )
        println(newUserEndpoint)
        val rq = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("newUser", "newUser")
            .build()
        val createUserRequest = Request.Builder().url(newUserEndpoint).post(rq).build()

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = okHttpClient.newCall(createUserRequest).execute()
                if (response.code == 200) {
                    updatePfp()
                } else {
                    // fail
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        _uiState.update { it.copy(isLoadingOverlayVisible = false) }
    }

    fun updatePfp() {
        val pictureUpload = userSession.cachedPicture.toFile()
        _uiState.update { it.copy(isLoadingOverlayVisible = true) }

        val requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(
                "file",
                "picture.png",
                pictureUpload.asRequestBody("image/png".toMediaType())
            )
            .build()

        val endpoint = Constants.ENDPOINT_UPLOAD.format(uiState.value.ra)

        val request = Request.Builder()
            .url(endpoint)
            .post(requestBody)
            .build()

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = okHttpClient.newCall(request).execute()
                if (response.code == 200) {
                    _uiState.update {
                        it.copy(
                            isLoadingOverlayVisible = false,
                            firstAccess = false
                        )
                    }
                    userSession.cachedPicture = Uri.EMPTY
                    loadPfp()
                } else {
                    log("Failed!")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun loadPfp() {
        val pictureUrl = Constants.ENDPOINT_PFP.format(uiState.value.ra)
        _uiState.update { it.copy(pictureUrl = pictureUrl) }
    }

    fun onRequestVisualizarCarteirinha(onFinishRequest: () -> Unit) {
        val ra = userSession.ra
        val certificateEndpoint = ENDPOINT_CERTIFICATE.format(ra)
        val rq = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("ra", "ra")
            .build()
        val endpointRequest = Request.Builder().url(certificateEndpoint).post(rq).build()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = okHttpClient.newCall(endpointRequest).execute()
                if (response.code == 200) {
                    val uuidStrip =
                        response.body!!.string()
                            .replace("{ \"uuid\": \"", "")
                            .replace("\" }", "")
                    userSession.digest = uuidStrip
                    withContext(Dispatchers.Main) {
                        onFinishRequest()
                    }
                } else {
                    // fail
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun onToggleBack() {
        val hasPressedBack = uiState.value.hasPressedBack.not()
        _uiState.update { it.copy(hasPressedBack = hasPressedBack) }
    }


}