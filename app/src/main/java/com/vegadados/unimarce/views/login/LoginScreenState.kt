package com.vegadados.unimarce.views.login

enum class LoginSection {
    NONE,
    CAPTCHA,
    LOGIN_FAILED,
    LOGIN_SUCCESS,
    NAVIGATE_TO_HOME
}

data class LoginScreenState(
    val username: String = "",
    val password: String = "",
    val isDeveloperOptionsEnabled: Boolean = false,
    val loginSection: LoginSection = LoginSection.NONE
)