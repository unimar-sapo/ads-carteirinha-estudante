package com.vegadados.unimarce.views.home

data class HomeScreenState(
    val ra: String = "",
    val name: String = "",
    val course: String = "",
    val classYear: String = "",
    val courseId: Int = -1,
    val pictureUrl: String = "",
    val firstAccess: Boolean = false,
    val isLoadingOverlayVisible: Boolean = true,
    val hasPressedBack: Boolean = false,
)