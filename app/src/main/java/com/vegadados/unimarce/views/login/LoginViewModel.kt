package com.vegadados.unimarce.views.login

import android.app.Application
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vegadados.unimarce.BaseWebView
import com.vegadados.unimarce.UserSession
import com.vegadados.unimarce.log
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel
@Inject constructor(
    val app: Application,
    val webView: BaseWebView,
    val userSession: UserSession
) : ViewModel() {
    fun onChangeUsernameField(text: String) {
        _uiState.update {
            it.copy(
                username =
                if (text.length > 11)
                    text.substring(0, 11)
                else
                    text
            )
        }
    }

    fun onChangePasswordField(text: String) {
        _uiState.update { it.copy(password = text) }
    }

    fun onClickLoginButton() {
        webView.addJavascriptInterface(this, "Android")
        webView.loadUrl("https://portal.unimar.br/alunos/index/login")
        _uiState.update { it.copy(loginSection = LoginSection.CAPTCHA) }
    }

    // called by JS code, ignore non-used lint
    @JavascriptInterface
    fun onReceiveUserDetails(rawUserDetails: String) {
        log("onReceiveUserDetails(): rawUserDetails=$rawUserDetails")
        // expect: rawUserDetails=1870000 - MATHEUS MENEZES - 4432 - SUP TEC ANALISE DES. SISTEMAS - 6A
        val arrayUserDetails = rawUserDetails.split(" - ")
        val ra = arrayUserDetails[0]
        val name = arrayUserDetails[1]
        val courseId = arrayUserDetails[2].toIntOrNull() ?: -1
        val courseName = arrayUserDetails[3]
        val classYear = arrayUserDetails[4]
        userSession.updateLocalSession(ra, name, courseName, classYear, courseId)
    }

    fun onPressBackButtonOnLoginLanding() {
        _uiState.update { it.copy(loginSection = LoginSection.NONE) }
    }

    fun onEnableDevOpts() {
        _uiState.update { it.copy(isDeveloperOptionsEnabled = true) }
    }

    private val _uiState = MutableStateFlow(LoginScreenState())
    val uiState: StateFlow<LoginScreenState> = _uiState.asStateFlow()

    init {
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                log("onPageFinished(): url=$url")

                val customJs = app.assets.open("append.js")
                    .bufferedReader().use { it.readText() }
                webView.loadUrl("javascript:$customJs;")

                if (url == "https://portal.unimar.br/alunos/index/login") {
                    val loginProcedureWrap = "loginProcedure(\"%s\", \"%s\")"
                        .format(uiState.value.username, uiState.value.password.uppercase())
                    webView.loadUrl("javascript:$loginProcedureWrap")
                    return
                }

                if (url == "https://portal.unimar.br/alunos/index") {
                    webView.loadUrl("https://portal.unimar.br/alunos/index/horaAula")
                    _uiState.update { it.copy(loginSection = LoginSection.LOGIN_SUCCESS) }
                }

                if (url == "https://portal.unimar.br/alunos/index/horaAula") {
                    viewModelScope.launch(Dispatchers.IO) {
                        // page load user info async, placeholders to "- - - -", assume 3s as an secure value.
                        Thread.sleep(3000)
                        launch(Dispatchers.Main) {
                            webView.loadUrl("javascript:Android.onReceiveUserDetails(fetchUserDetails());")
                            _uiState.update { it.copy(loginSection = LoginSection.NAVIGATE_TO_HOME) }
                        }
                    }
                }
            }
        }
    }

}
