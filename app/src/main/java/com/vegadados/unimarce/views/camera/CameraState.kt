package com.vegadados.unimarce.views.camera

import android.net.Uri

data class CameraState(
    val takenPhotoUri: Uri = Uri.EMPTY,
    val qrScannedValue: String = ""
)