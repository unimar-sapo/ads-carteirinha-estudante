package com.vegadados.unimarce.views.camera

import android.app.Application
import android.net.Uri
import android.util.Log
import androidx.camera.core.ImageCaptureException
import androidx.lifecycle.ViewModel
import com.vegadados.unimarce.UserSession
import com.vegadados.unimarce.log
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject

@HiltViewModel
class CameraViewModel
@Inject constructor(
    val app: Application,
    val userSession: UserSession
) : ViewModel() {
    private val _uiState = MutableStateFlow(CameraState())
    val uiState: StateFlow<CameraState> = _uiState.asStateFlow()
    var cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
    val cacheDir = app.cacheDir

    fun onImageCaptured(uri: Uri) {
        _uiState.update { it.copy(takenPhotoUri = uri) }
    }

    fun onImageCaptureError(e: ImageCaptureException) {
        log(e.stackTraceToString())
    }

    fun onQrCodeScanned(somethingScanned: String) {
        _uiState.update { it.copy(qrScannedValue = somethingScanned) }
    }

    fun onContinue() {
        userSession.cachedPicture = uiState.value.takenPhotoUri
    }

}