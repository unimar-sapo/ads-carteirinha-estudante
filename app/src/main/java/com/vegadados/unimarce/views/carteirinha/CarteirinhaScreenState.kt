package com.vegadados.unimarce.views.carteirinha

data class CarteirinhaScreenState(
     val digest: String = ""
)