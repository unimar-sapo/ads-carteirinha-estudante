package com.vegadados.unimarce.views.camera

import android.annotation.SuppressLint
import android.util.Log
import androidx.annotation.OptIn
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.common.InputImage
import com.vegadados.unimarce.BuildConfig

@OptIn(ExperimentalGetImage::class)
@SuppressLint("UnsafeExperimentalUsageError")
fun processImageProxy(
    barcodeScanner: BarcodeScanner,
    imageProxy: ImageProxy,
    onFoundQr: (String) -> Unit
) {
    val inputImage =
        InputImage.fromMediaImage(imageProxy.image!!, imageProxy.imageInfo.rotationDegrees)

    barcodeScanner.process(inputImage)
        .addOnSuccessListener { barcodes ->
            barcodes.forEach {
                onFoundQr(it.rawValue ?: "")
            }
        }
        .addOnFailureListener {
            Log.e(BuildConfig.APPLICATION_ID, it.message ?: it.toString())
        }.addOnCompleteListener {
            imageProxy.close()
        }
}