package com.vegadados.unimarce.views.carteirinha

import android.app.Application
import androidx.lifecycle.ViewModel
import com.vegadados.unimarce.BaseWebView
import com.vegadados.unimarce.UserSession
import com.vegadados.unimarce.log
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class CarteirinhaViewModel
@Inject constructor(
    val app: Application,
    val webView: BaseWebView,
    val userSession: UserSession
) : ViewModel() {

    private val _uiState = MutableStateFlow(CarteirinhaScreenState())
    val uiState: StateFlow<CarteirinhaScreenState> = _uiState.asStateFlow()

    fun onLoaded() {
        _uiState.update { it.copy(digest = userSession.digest) }
    }

    init {
        log("loadedSession: $userSession")
    }

}