package com.vegadados.unimarce.views.login

import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBars
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowForward
import androidx.compose.material.icons.outlined.Badge
import androidx.compose.material.icons.outlined.Password
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import com.vegadados.unimarce.R
import com.vegadados.unimarce.components.LoadingOverlay
import kotlinx.coroutines.flow.collectLatest

@Composable
fun LoginScreen(
    loginVm: LoginViewModel = hiltViewModel(),
    navigateHomeScreen: () -> Unit,
    navCameraScreen: () -> Unit = {},
    navCarteirinhaScreen: () -> Unit = {},
    navScanQr: () -> Unit = {},
) {
    val uiState by loginVm.uiState.collectAsState()
    val context = LocalContext.current

    val insets = WindowInsets
        .systemBars
        .asPaddingValues()

    val uriHandler = LocalUriHandler.current

    LaunchedEffect(Unit) {
        snapshotFlow { uiState.loginSection }.collectLatest { value ->
            when (value) {
                LoginSection.CAPTCHA ->
                    Toast.makeText(
                        context,
                        "Por favor complete o captcha.",
                        Toast.LENGTH_LONG
                    ).show()

                LoginSection.NAVIGATE_TO_HOME ->
                    navigateHomeScreen()

                else -> {}
            }
        }
    }

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        Column(modifier = Modifier.padding()) {
            if (uiState.loginSection == LoginSection.CAPTCHA) {
                AndroidView(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(insets),
                    factory = {
                        loginVm.webView.apply {
                            layoutParams = ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT
                            )
                        }
                    }, update = {
                        //it.loadUrl("0.0.0.0")
                    })
                BackHandler { loginVm.onPressBackButtonOnLoginLanding() }
            } else {
                Spacer(
                    modifier = Modifier
                        .padding(insets)
                        .fillMaxSize()
                        .weight(0.5f)
                )
                Box(modifier = Modifier.padding(start = 24.dp, end = 24.dp)) {
                    Image(
                        modifier = Modifier
                            .clickable { loginVm.onEnableDevOpts() }
                            .fillMaxWidth(),
                        painter = painterResource(R.drawable.login_banner),
                        contentDescription = ""
                    )
                }
                Spacer(modifier = Modifier.padding(4.dp))
                Row(
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    TextButton(onClick = { uriHandler.openUri("https://www.unimar.br/") }) {
                        Text(text = "Acessar site")
                    }
                    Spacer(modifier = Modifier.padding(4.dp))
                    TextButton(onClick = { navScanQr() }) {
                        Text(text = "Verificar carteirinha")
                    }
                }
                Spacer(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(0.5f)
                )
                Column(Modifier.padding(24.dp)) {
                    Text(
                        text = "Entre com sua conta UNIMAR",
                        fontSize = 14.sp,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.padding(4.dp))
                    CustomTextField(
                        title = "Digite seu CPF:",
                        inputTextContent = uiState.username,
                        icon = Icons.Outlined.Badge,
                        onChangedValue = loginVm::onChangeUsernameField,
                        customTextMask = CustomTextMask.CPF
                    )
                    Spacer(modifier = Modifier.padding(8.dp))
                    CustomTextField(
                        title = "Digite sua senha:",
                        inputTextContent = uiState.password,
                        icon = Icons.Outlined.Password,
                        onChangedValue = loginVm::onChangePasswordField,
                        customTextMask = CustomTextMask.PASSWORD
                    )
                    Spacer(modifier = Modifier.padding(2.dp))
                    Row {
                        Spacer(
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(1f)
                        )
                        TextButton(onClick = loginVm::onClickLoginButton) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Text(text = "Entrar")
                                Spacer(modifier = Modifier.padding(2.dp))
                                Icon(
                                    imageVector = Icons.Outlined.ArrowForward,
                                    contentDescription = null
                                )
                            }
                        }
                    }
                    if (uiState.isDeveloperOptionsEnabled) {
                        Text(text = "Developer options enabled!")
                        Row(
                            modifier = Modifier.horizontalScroll(rememberScrollState()),
                            horizontalArrangement = Arrangement.spacedBy(2.dp)
                        ) {
                            Button(onClick = {
                                loginVm.userSession.initializeFakeSession()
                                navigateHomeScreen()
                            }) {
                                Text(text = "Home Screen")
                            }
                            Button(onClick = { navCameraScreen() }) {
                                Text(text = "Camera Screen")
                            }
                            Button(onClick = { navCarteirinhaScreen() }) {
                                Text(text = "Carteirinha Screen")
                            }
                            Button(onClick = { navScanQr() }) {
                                Text(text = "QR Screen")
                            }
                        }
                    }
                    Box(modifier = Modifier.imePadding())
                }
            }
        }
    }

    if (uiState.loginSection == LoginSection.LOGIN_SUCCESS) {
        LoadingOverlay()
    }
}

enum class CustomTextMask {
    NONE, CPF, PASSWORD
}

@Composable
fun CustomTextField(
    title: String,
    inputTextContent: String,
    icon: ImageVector? = null,
    onChangedValue: (String) -> Unit,
    customTextMask: CustomTextMask = CustomTextMask.NONE
) {
    Box {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .clip(
                    RoundedCornerShape(
                        topStart = 0.dp,
                        topEnd = 24.dp,
                        bottomEnd = 24.dp,
                        bottomStart = 24.dp
                    )
                )
                .background(MaterialTheme.colorScheme.primary.copy(alpha = 0.1f))
        ) {
            Spacer(modifier = Modifier.padding(8.dp))
            if (icon != null)
                Icon(imageVector = icon, contentDescription = null)
            Spacer(modifier = Modifier.padding(8.dp))
            if (customTextMask == CustomTextMask.PASSWORD)
                TextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clip(CircleShape),
                    value = inputTextContent,
                    colors = TextFieldDefaults.colors(
                        disabledTextColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent,
                    ),
                    onValueChange = onChangedValue,
                    visualTransformation = PasswordVisualTransformation()
                )
            else
                TextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clip(CircleShape),
                    value = inputTextContent,
                    colors = TextFieldDefaults.colors(
                        disabledTextColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent,
                    ),
                    onValueChange = onChangedValue,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    visualTransformation = {
                        when (customTextMask) {
                            CustomTextMask.CPF ->
                                cpfMask(it)

                            else ->
                                TransformedText(it, OffsetMapping.Identity)
                        }
                    },
                )
        }
        Box(
            modifier = Modifier
                .padding(bottom = 48.dp)
                .clip(RoundedCornerShape(12.dp))
                .background(MaterialTheme.colorScheme.secondaryContainer)
        ) {
            Text(
                text = title,
                modifier = Modifier.padding(12.dp),
                color = MaterialTheme.colorScheme.onSecondaryContainer
            )
        }
    }
}

fun cpfMask(text: AnnotatedString): TransformedText {

    // XXX.XXX.XXX-XX
    val trimmed = if (text.text.length >= 11) text.text.substring(0..10) else text.text
    var out = ""
    for (i in trimmed.indices) {
        out += trimmed[i]
        if (i == 2) out += "."
        if (i == 5) out += "."
        if (i == 8) out += "-"
    }

    val numberOffsetTranslator = object : OffsetMapping {
        override fun originalToTransformed(offset: Int): Int {
            if (offset <= 2) return offset
            if (offset <= 5) return offset + 1
            if (offset <= 8) return offset + 2
            if (offset <= 10) return offset + 3
            return 14
        }

        override fun transformedToOriginal(offset: Int): Int {
            if (offset <= 3) return offset
            if (offset <= 6) return offset - 1
            if (offset <= 9) return offset - 2
            if (offset <= 11) return offset - 3
            return 10
        }
    }

    return TransformedText(AnnotatedString(out), numberOffsetTranslator)
}