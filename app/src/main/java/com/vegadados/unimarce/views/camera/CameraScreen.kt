package com.vegadados.unimarce.views.camera

import android.net.Uri
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun CameraScreen(
    isScanQr: Boolean = false,
    cameraVm: CameraViewModel = hiltViewModel(),
    navigateUp: () -> Unit,
) {
    val uiState by cameraVm.uiState.collectAsState()
    val ctx = LocalContext.current

    val cameraPermissionState = rememberPermissionState(
        android.Manifest.permission.CAMERA
    )

    if (uiState.takenPhotoUri == Uri.EMPTY && uiState.qrScannedValue.isEmpty()) {
        when (cameraPermissionState.status) {
            PermissionStatus.Granted -> {
                CameraView(
                    isScanQrMode = isScanQr,
                    onScannedQr = { cameraVm.onQrCodeScanned(it) },
                    indicatorText = if (isScanQr) "Aponte a câmera para o código QR da carteirinha" else "Toque no botão abaixo para tirar uma foto",
                    outputDirectory = cameraVm.cacheDir,
                    executor = cameraVm.cameraExecutor,
                    onImageCaptured = cameraVm::onImageCaptured,
                    onError = cameraVm::onImageCaptureError
                )
            }

            is PermissionStatus.Denied -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .navigationBarsPadding()
                        .statusBarsPadding()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    val textToShow =
                        if ((cameraPermissionState.status as PermissionStatus.Denied).shouldShowRationale) {
                            "Permissão de câmera necessária.."
                        } else {
                            "Permissão de câmera é necessária.\nPor favor permita a permissão de câmera tocando no botão a seguir"
                        }
                    Spacer(modifier = Modifier.padding(8.dp))
                    Text(textToShow, color = MaterialTheme.colorScheme.onSurface)
                    Button(onClick = { cameraPermissionState.launchPermissionRequest() }) {
                        Text("Permitir acesso a câmera")
                    }
                }
            }
        }
    } else {
        if (uiState.qrScannedValue.isNotEmpty()) {
            AndroidView(
                modifier = Modifier.fillMaxSize(),
                factory = {
                    val webview = WebView(ctx).apply {
                        webViewClient = WebViewClient()
                        loadUrl(uiState.qrScannedValue)
                    }
                    webview.settings.javaScriptEnabled = true
                    webview.apply {
                        layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                    }
                })
            BackHandler {
                cameraVm.onQrCodeScanned("")
            }
        } else {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                AsyncImage(
                    modifier = Modifier
                        .size(600.dp)
                        .padding(8.dp),
                    model = uiState.takenPhotoUri,
                    contentDescription = "User photo"
                )
                Spacer(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                )
                Button(
                    modifier = Modifier.padding(12.dp),
                    onClick = {
                        cameraVm.onContinue()
                        navigateUp()
                    }
                ) {
                    Text(text = "Continuar?")
                }
                if (uiState.takenPhotoUri != Uri.EMPTY) {
                    BackHandler {
                        cameraVm.onImageCaptured(Uri.EMPTY)
                    }
                }
            }
        }
    }

}