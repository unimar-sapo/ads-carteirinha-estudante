package com.vegadados.unimarce

object Constants {
    const val API_BASE_URL = "https://mainline.vegadados.com"
    const val ENDPOINT_USER_DETAILS = "$API_BASE_URL/userinfo/%s"
    const val ENDPOINT_UPLOAD = "$API_BASE_URL/upload?ra=%s"
    const val ENDPOINT_PFP = "$API_BASE_URL/uploads/%s.png"
    const val ENDPOINT_NEW_USER = "$API_BASE_URL/newuser?ra=%s&name=%s&course=%s&courseYear=%s&courseId=%s"
    const val ENDPOINT_CERTIFICATE = "$API_BASE_URL/certificate/%s"
    const val ENDPOINT_CARTEIRA = "$API_BASE_URL/carteira?digest=%s"
}