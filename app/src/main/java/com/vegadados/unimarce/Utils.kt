package com.vegadados.unimarce

import android.graphics.Bitmap
import android.util.Log
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.util.UUID


fun log(text: String) {
    Log.d(BuildConfig.APPLICATION_ID, text)
}

fun storeImage(image: Bitmap) {
    val uuid = UUID.randomUUID().toString() + ".png"
    val file = "/data/data/" + BuildConfig.APPLICATION_ID + "/files/" + uuid
    val pictureFile = File(file)

    try {
        val fos = FileOutputStream(pictureFile)
        image.compress(Bitmap.CompressFormat.PNG, 90, fos)
        fos.close()
    } catch (e: FileNotFoundException) {
        Log.d("TAG", "File not found: " + e.message)
    } catch (e: IOException) {
        Log.d("TAG", "Error accessing file: " + e.message)
    }
}

fun Bitmap.convertToByteArray(): ByteArray {
    //minimum number of bytes that can be used to store this bitmap's pixels
    val size = this.byteCount

    //allocate new instances which will hold bitmap
    val buffer = ByteBuffer.allocate(size)
    val bytes = ByteArray(size)

    //copy the bitmap's pixels into the specified buffer
    this.copyPixelsToBuffer(buffer)

    //rewinds buffer (buffer position is set to zero and the mark is discarded)
    buffer.rewind()

    //transfer bytes from buffer into the given destination array
    buffer.get(bytes)

    //return bitmap's pixels
    return bytes
}