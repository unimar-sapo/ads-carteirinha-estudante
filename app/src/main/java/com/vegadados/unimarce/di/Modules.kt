package com.vegadados.unimarce.di

import android.content.Context
import com.vegadados.unimarce.BaseWebView
import com.vegadados.unimarce.UserSession
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class Modules {

    @Singleton
    @Provides
    fun provideWebView(
        @ApplicationContext appContext: Context
    ): BaseWebView {
        val webView = BaseWebView(appContext)
        println(webView.settings)
        webView.clearCache(true)
        return webView
    }

    @Singleton
    @Provides
    fun provideUserSession(): UserSession {
        return UserSession()
    }

    @Singleton
    @Provides
    fun provideOkHttp(): OkHttpClient {
        return OkHttpClient()
    }

}