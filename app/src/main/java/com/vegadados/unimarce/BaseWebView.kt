package com.vegadados.unimarce

import android.content.Context
import android.webkit.WebView

open class BaseWebView(context: Context) : WebView(context) {
    init {
        settings.javaScriptEnabled = true
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.domStorageEnabled = true
    }
}