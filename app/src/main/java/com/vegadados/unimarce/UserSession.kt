package com.vegadados.unimarce

import android.net.Uri

class UserSession(
    var ra: String = "",
    var name: String = "",
    var course: String = "",
    var classYear: String = "",
    var courseId: Int = -1,
) {

    var cachedPicture = Uri.EMPTY
    var digest: String = ""

    fun updateLocalSession(
        ra: String = "",
        name: String = "",
        course: String = "",
        classYear: String = "",
        courseId: Int = -1
    ) {
        this.ra = ra
        this.name = name
        this.course = course
        this.courseId = courseId
        this.classYear = classYear
    }

    // Dummy data for testing and debugging purposes
    fun initializeFakeSession() {
        updateLocalSession(
            ra = "11111",
            name = "John Doe",
            course = "Foo Bar",
            classYear = "6A",
            courseId = 1111
        )
    }

    init {
        // For testing and debugging purposes, init shouldn't call this method
        // initializeFakeSession()
    }

    override fun toString(): String {
        return "ra: $ra, name: $name, course: $course, classYear: $classYear, courseId: $courseId"
    }

}