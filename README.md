Carteira de estudante digital, projeto feito para a materia de estágio supervisionado, curso da UNIMAR de Análise e Desenvolvimento de Sistemas.

Personal project, there is no real-world usage for this (at least by now), made for my university degree, it is a "digital student id" made specifically to work with UNIMAR university.

---

### Antes de clonar

O app está configurado para usar minha api rodando no meu servidor, em um endereço hardcodado: [Constants.kt](https://gitlab.com/unimar-sapo/ads-carteirinha-estudante/-/blob/master/app/src/main/java/com/vegadados/unimarce/Constants.kt?ref_type=heads#L4)

Para usar sua própria API, utilize [Repositório da API](https://gitlab.com/unimar-sapo/api).

Com sua API de "pé", altere o arquivo `Constants.kt` para o endereço da sua API.

---

### Instalando app pelo Android Studio
1. Instalar Android Studio
2. Clonar repo `git clone https://gitlab.com/unimar-sapo/ads-carteirinha-estudante`
3. Abrir repo no Android Studio
4. Tocar no botão de Play

É possível gerar o apk do app indo em "Build" -> "Build bundle/apk" -> "Build APK"; ou "Generate signined bundle/apk" para gerar um aplicativo de lançamento e assinado.

### Instalando app pelo Gradle
1. Instalar Java 17 (testado com OpenJDK 17) e baixar Android SDK (command line tools)
2. No terminal: `export ANDROID_SDK_ROOT=/caminho/do/android/sdk`
3. Gerar apk debug: `./gradlew assembleDebug`

É possível gerar um apk de lançamento, com assinado com debug keys, por meio do `./gradlew assembleRelease`, caso queria gerar um apk com release keys, é necessário criar sua propria chave para assinatura (keystore).

** Para compilação do app no Windows, export não deve funcionar, como não sei se existe algo equivaliente, nesse caso, é necessário adicionar o ANDROID_SDK_ROOT nas variáveis do ambiente, além disso, o gradlew deve ser referenciado como `.\gradlew.bat`.

---

Além disso, distribuimos aplicativos prontos do projeto, na pasta apks.
